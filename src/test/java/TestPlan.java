import io.restassured.RestAssured;
import io.restassured.response.Response;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import org.apache.http.HttpRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.jmeter.extractor.BeanShellPostProcessor;
import org.apache.jmeter.extractor.XPath2Extractor;
import org.apache.jmeter.extractor.XPathExtractor;
import org.apache.jmeter.protocol.http.curl.BasicCurlParser;
import org.apache.jmeter.protocol.http.util.HTTPConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;
import us.abstracta.jmeter.javadsl.JmeterDsl;
import us.abstracta.jmeter.javadsl.core.DslTestPlan;
import us.abstracta.jmeter.javadsl.core.TestPlanStats;
import us.abstracta.jmeter.javadsl.core.engines.EmbeddedJmeterEngine;
import us.abstracta.jmeter.javadsl.core.listeners.ResponseFileSaver;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.apache.jmeter.extractor.XPathExtractor.*;
import static us.abstracta.jmeter.javadsl.JmeterDsl.*;

public class TestPlan {

	String req = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws.ustorage.idmt.ru\">\n" +
			"    <soapenv:Header>\n" +
			"        <ServiceContext xmlns=\"http://context.core.datamodel.fs.documentum.emc.com/\" xmlns:ns2=\"http://properties.core.datamodel.fs.documentum.emc.com/\" xmlns:ns3=\"http://profiles.core.datamodel.fs.documentum.emc.com/\">\n" +
			"            <Identities password=\"Ppkuek44\" repositoryName=\"SHD\" userName=\"sap_user\" xsi:type=\"RepositoryIdentity\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
			"        </ServiceContext>\n" +
			"    </soapenv:Header>\n" +
			"    <soapenv:Body>\n" +
			"        <ws:createDocument>\n" +
			"            <!--Optional:-->\n" +
			"            <document>\n" +
			"                <objectName>Нагрузочное тестирование</objectName>\n" +
			"                <title></title>\n" +
			"                <objectType>ddt_006</objectType>\n" +
			"                <contentSize>0</contentSize>\n" +
			"                <contentType>docx</contentType>\n" +
			"                <creatorName>Нагрузка</creatorName>\n" +
			"                <sourceSystemCodes>FET</sourceSystemCodes>\n" +
			"                <balanceUnitCode>0100</balanceUnitCode>\n" +
			"                <electronicSignature>false</electronicSignature>\n" +
			"                <extAttrValues>\n" +
			"                    <name>s_organizaciya</name>\n" +
			"                    <values>нагрузочное тестирование ${__Random(1,50000,)}</values>\n" +
			"                </extAttrValues>\n" +
			"                <extAttrValues>\n" +
			"                    <name>t_data_dokumenta</name>\n" +
			"                    <values>${__time(dd.MM.yyyy HH:mm:ss,currentTime)}</values>\n" +
			"                </extAttrValues>\n" +
			"            </document>\n" +
			"        </ws:createDocument>\n" +
			"    </soapenv:Body>\n" +
			"</soapenv:Envelope>";

	private static final Integer THREADS = 1;
	private static final Integer ITERATIONS = 1;

	@Test
	public void createIdList() throws IOException {
		String response = "";
		testPlan(
				threadGroup(THREADS, ITERATIONS,
						httpSampler("http://192.168.42.106/document-ws/DocumentService")
								.method(HTTPConstants.POST)
								.contentType(ContentType.TEXT_XML)
								.contentType(ContentType.TEXT_XML.withCharset("UTF-8"))
								.bodyFile("F:\\WORK\\ID-TM\\jmeter\\src\\test\\resources\\request.xml")
								.children(
										boundaryExtractor("docId", "<objectId>", "<")
								)


		)).run();

	}


	@Test
	public void createContent() throws IOException {
		TestPlanStats run = testPlan(threadGroup(THREADS, ITERATIONS,
						httpSampler("http://192.168.42.106/document-ws/DocumentService")
								.method(HTTPConstants.POST)
								.contentType(ContentType.TEXT_XML)
								.contentType(ContentType.TEXT_XML.withCharset("UTF-8"))
								.bodyFile("E:\\JavaProjects\\WorkProjects\\Jmeter\\src\\test\\resources\\requestForCreateContent.xml")
				),
				resultsTreeVisualizer()
		)
				.run();

	}

	@Test
	public void getIdFromResponse(){
		Response response = given()
				.header("Content-type", "text/xml")
				.and()
				.body(req)
				.post("http://192.168.42.106/document-ws/DocumentService")
				.then()
				.extract().response();
		System.out.println(response.getBody().xmlPath().get().attributes().get("objectId"));

	}



}



